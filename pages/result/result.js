// pages/result/result.js
const app = getApp()
Page({
  /**
   * 页面的初始数据
   */
  data: {
    score: 0,
    maxScore: 0,
    remainTimes: 2,
  },

  restart: () => {
    wx.redirectTo({
      url: '/pages/rule/rule',
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.request({
      url: "https://www.ylinxin.com/getExam",
      method: "POST",
      header:{
        'token':app.globalData.token
      },
      success: res => {
        console.log(res)
        const { times, scores } = res.data.data;
        console.log(times, scores);
        //历史最高成绩
        let maxScore = 0;
        for(let score of scores) {
          console.log(score);
          maxScore = Math.max(maxScore, score)
        }
        //本次成绩
        let score = scores[0];
        this.setData({
          score,
          maxScore,
          // remainTimes: 3 - times
           remainTimes: 3-times
        })
      },
    });
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {},

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {},

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {},

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {},

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {},

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {},

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {},
});
