// pages/rule/rule.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    confirm: true,
    canTest:true,
  },

  change() {
    this.setData({confirm: !this.data.confirm});
  },
  throttle(fnc){
    let canRun = true;
    return function(){
        if(!canRun){
            return
        }
        canRun =false;
        setTimeout(function(){
          fnc.apply(this,arguments)
            canRun=true;
        },2000)
    }
},

  rule(){
    var times = ''
    wx.request({
      url: "https://www.ylinxin.com/getExam",
      method: "POST",
      header:{
        'token':app.globalData.token},
      success: res => {
        times = res.data.data.times
        console.log(res)
        if(times>=3){
          this.setData({
            canTest:false
          })
        }
        if(this.data.canTest){
          wx.setStorageSync('hour', 1)
          wx.setStorageSync('min', 0)
          wx.setStorageSync('sec', 0)
          wx.navigateTo({
            url: '/pages/subject/subject',
          })
          console.log('nav')
        }else{
          wx.showToast({
            title: '已答题三次',
            icon:'error'
          })
        }
      }
      
    });
    
    
  },
  RRule(){
    this.throttle(this.rule())
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})