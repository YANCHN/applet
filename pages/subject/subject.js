// index.js
// 获取应用实例
const app = getApp()
const throttle = require('../../utils/util')
Page({
  onShareAppMessage() {
    return {
      title: 'radio',
      path: 'page/component/pages/radio/radio'
    }
  },
  data: {
    TimeID: -1,
    CustomBar: app.globalData.CustomBar,
    setInter: '',
    hour: 1,
    min: 0,
    sec: 0,
    id: 1,
    ques: "",
    type: "1",
    // optionA:{value:"A",name:"AAAAAAAA"},
    // optionB:{value:"B",name:"BBBB"},
    // optionC:{value:"C",name:"CCCC"},
    // optionD:{value:"D",name:"DDDD"},
    // optionE:{value:"E",name:"DDDD"},
    // choose:[0,0,0,0,1,1,1,1,0,0,0,0,1,1,1,1,0,0,0,0,1,1,1,1],

    optionA: { value: "", name: "" },
    optionB: { value: "", name: "" },
    optionC: { value: "", name: "" },
    optionD: { value: "", name: "" },
    optionE: { value: "", name: "" },
    choose: [],
    answer: '',
    cansubmit: false
  },

  onLoad() {
    console.log("aaa")
    if (wx.getStorageSync('answer')) {
      app.globalData.answer = wx.getStorageSync('answer')
    }
    if (wx.getStorageSync('choosed')) {
      app.globalData.choosed = wx.getStorageSync('choosed')
    }
    var globalChoose = app.globalData.choosed;
    console.log(app.globalData.choosed)
    this.setData({
      choose: globalChoose,
      id: app.globalData.subjectId,
    })
    console.log(this.data.choose)
    var that = this;


    console.log(app.globalData.answer)
    wx.request({
      url: 'https://www.ylinxin.com/getExamQuestions',
      header: {
        'token': app.globalData.token
      },
      method: "POST",
      success: (res) => {
        console.log(res)
        if (res.data.code != 200) {
          wx.showToast({
            title: '获取题目失败',
            icon: 'error',
            mask: true
          })
        } else {

          this.setData({
            ques: res.data.data[that.data.id - 1].stem,
            optionA: { value: 'A', name: res.data.data[that.data.id - 1].optionA },
            optionB: { value: 'B', name: res.data.data[that.data.id - 1].optionB },
            optionC: { value: 'C', name: res.data.data[that.data.id - 1].optionC },
            optionD: { value: 'D', name: res.data.data[that.data.id - 1].optionD },
            optionE: { value: 'E', name: res.data.data[that.data.id - 1].optionE },
            type: res.data.data[that.data.id - 1].type,
            answer: app.globalData.answer[that.data.id - 1]
          })
        }
      },
      fail() {

      }
    })
    if (wx.getStorageSync('hour') != null) {
      this.setData({
        hour: wx.getStorageSync('hour'),
        min: wx.getStorageSync('min'),
        sec: wx.getStorageSync('sec')
      })

    }

    this.startSetInter();
    this.submit = throttle.throttle(this._submit.bind(this), 2000)
  },

  radioChange(e) {
    console.log(e.detail.value)
    app.globalData.choosed[this.data.id - 1] = 1
    var value = e.detail.value
    if (typeof value == 'object') {
      app.globalData.answer[this.data.id - 1] = e.detail.value.join('')
    } else {
      app.globalData.answer[this.data.id - 1] = e.detail.value
    }

    this.setData({
      choose: app.globalData.choosed,
    })
    console.log(app.globalData.choosed)
    wx.setStorageSync('answer', app.globalData.answer)
    wx.setStorageSync('choosed', app.globalData.choosed)
  },
  changeSubject(e) {
    console.log(e.currentTarget.dataset['subject'])
    app.globalData.subjectId = e.currentTarget.dataset['subject']
    clearInterval(this.data.setInter)
    this.onLoad()
  },
  showModal(e) {
    console.log('tap')
    this.setData({
      modalName: e.currentTarget.dataset.target
    })
  },
  hideModal(e) {
    this.setData({
      modalName: null
    })
  },
  // 计时
  //开始订单计时器
  startSetInter: function () {
    const that = this
    var second = that.data.sec
    var minute = that.data.min
    var hours = that.data.hour
    that.data.setInter = setInterval(function () { // 设置定时器
      second--
      if (second < 0) {
        second = 59 //  大于等于60秒归零
        minute--
        if (minute < 0) {
          minute = 59 //  大于等于60分归零
          hours--
          that.setData({
            hour: (hours < 10) ? ('0' + hours) : hours, //小时
          })
        }
        that.setData({
          min: (minute < 10) ? ('0' + minute) : minute, //分
        })
      }
      that.setData({
        sec: (second < 10) ? ('0' + second) : second, //秒
      })
      wx.setStorageSync('hour', that.data.hour)
      wx.setStorageSync('min', that.data.min)
      wx.setStorageSync('sec', that.data.sec)
      if (that.data.hour == 0 && that.data.min == 0 && that.data.sec == 0) {
        console.log("在这里提交");
        var answerList = []
        for (var i = 0; i < 20; i++) {
          answerList.push({ questionId: i + 1, answer: app.globalData.answer[i] })
        }
        clearTimeout(that.TimeID);
        that.TimeID = setTimeout(() => {
          wx.request({
            url: 'https://www.ylinxin.com/uploadAnswer',
            header: {
              'token': app.globalData.token
            },
            method: "POST",
            data: answerList,
            success: (res) => {
              if (res.data.code != 200) {
                wx.showToast({
                  title: res.data.message,
                  icon: 'error',
                  mask: true
                })
              } else {
                wx.clearStorageSync()
                app.globalData.choosed = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
                app.globalData.answer = ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '']
                app.globalData.subjectId = 1
                wx.redirectTo({
                  url: '../result/result?score',
                })
              }

            },
          })
        }, 100);
      }
    }, 1000)

  },
  setsession() {

  },
  _submit() {
    this.setData({
      cansubmit: true
    })
    console.log(this.data);

    var answerList = []
    for (var i = 0; i < 20; i++) {
      answerList.push({ questionId: i + 1, answer: app.globalData.answer[i] })
    }
    var flag = true
    for (var i = 0; i < answerList.length; i++) {
      if (answerList[i].answer == '') {
        flag = false
      }
    }
    var that = this
    if (this.data.cansubmit) {
      if (flag == false) {
        wx.showModal({
          title: '提示',
          content: '还有题目没有答完哦，是否提交',
          success(res) {
            if (res.confirm) {
              clearTimeout(that.TimeID);
              that.TimeID = setTimeout(() => {
                wx.request({
                  url: 'https://www.ylinxin.com/uploadAnswer',
                  header: {
                    'token': app.globalData.token
                  },
                  method: "POST",
                  data: answerList,
                  success: (res) => {
                    if (res.data.code != 200) {
                      wx.showToast({
                        title: res.data.message,
                        icon: 'error',
                        mask: true
                      })
                    } else {
                      wx.clearStorageSync()
                      app.globalData.choosed = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
                      app.globalData.answer = ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '']
                      app.globalData.subjectId = 1
                      wx.redirectTo({
                        url: '../result/result?score',
                      })
                    }

                  },
                })
                that.setData({
                  cansubmit: false
                })
              }, 100);
            } else if (res.cancel) {
              console.log('用户点击取消')
              that.setData({
                cansubmit: false
              })
            }
          }
        })
      } else {
        clearTimeout(that.TimeID);
        that.TimeID = setTimeout(() => {
          wx.request({
            url: 'https://www.ylinxin.com/uploadAnswer',
            header: {
              'token': app.globalData.token
            },
            method: "POST",
            data: answerList,
            success: (res) => {
              if (res.data.code != 200) {
                wx.showToast({
                  title: res.data.message,
                  icon: 'error',
                  mask: true
                })
              } else {
                wx.clearStorageSync()
                app.globalData.choosed = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
                app.globalData.answer = ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '']
                app.globalData.subjectId = 1
                wx.redirectTo({
                  url: '../result/result?score',
                })
              }

            },
          })
          that.setData({
            cansubmit: false
          })
        }, 100);
      }

    }


  },
  nextQues() {
    app.globalData.subjectId = this.data.id + 1
    clearInterval(this.data.setInter)
    this.onLoad()
  },

})
