// pages/login/login.js
Page({
  /**
   * 页面的初始数据
   */
  data: {
    username:"",
    password:"",
    isShow:false,
  },

  userNameInp:function(e){
    this.setData({
      username: e.detail.value
    })
  },

  usePasswordInp:function(e){
    this.setData({
      password: e.detail.value
    })
  },

  show:function() {
    this.setData({
      isShow: !this.data.isShow
    })
  },
  
 throttle(fnc){
    let canRun = true;
    return function(){
        if(!canRun){
            return
        }
        canRun =false;
        setTimeout(function(){
          fnc.apply(this,arguments)
            canRun=true;
        },1000)
    }
},
loadModal(){
  this.setData({
    loadModal: true
  })
  setTimeout(()=> {
    this.setData({
      loadModal: false
    })
  }, 2000)
},
LLogin(){
  this.throttle(this.login())
},
//  throttle:function() {            
//     var timer = null;            
//     return function() {                
//         var context = this;               
//         var args = arguments;                
//         if (!timer) {                    
//             timer = setTimeout(function() {                        
//                 this.login.apply(context, args);                        
//                 timer = null;                    
//             }, 500);                
//         }            
//     }        
// },        
  
  login(){
    this.loadModal()
    var that = this;
    wx.request({
      url: 'https://www.ylinxin.com/login?username='+that.data.username+'&password='+that.data.password,
      method:"POST",
      // data:{
      //   username: that.data.username,
      //   password: that.data.password,
      // },
      success(res){
        console.log(res);
        var app = getApp()
        app.globalData.token=res.data.data
        if(res.data.code === 200) {
          wx.redirectTo({
          url: '/pages/rule/rule',
        })
        }else {
          wx.showToast({
                title: '账号或密码错误',
                icon:'error'
              })
        }
        
      },
      fail(res) {
        console.log(res);
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})