const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return `${[year, month, day].map(formatNumber).join('/')} ${[hour, minute, second].map(formatNumber).join(':')}`
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : `0${n}`
}
function throttle(fn, gapTime) {
  let lastTimes = null
  return function (e) {
    let _nowTime = new Date()
    if (!lastTimes || _nowTime - lastTimes > gapTime ) {
      fn(e);
      lastTimes = _nowTime
    }
  }
}
module.exports = {
  formatTime,
  throttle
}
